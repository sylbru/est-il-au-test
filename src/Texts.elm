module Texts exposing (..)

import Locale exposing (Locale(..))


iHaveHadNoDrinks : Locale -> String
iHaveHadNoDrinks locale =
    case locale of
        Fr ->
            "Je n’ai rien bu."

        En ->
            "I’ve had no drinks."


aDrinkAt : Locale -> String
aDrinkAt locale =
    case locale of
        Fr ->
            "Un verre à "

        En ->
            "A drink at "


aDrinkAtColon : Locale -> String
aDrinkAtColon locale =
    case locale of
        Fr ->
            "J’ai fini mon verre à\u{00A0}:"

        En ->
            "I finished my drink at:"


earlier : Locale -> String
earlier locale =
    case locale of
        Fr ->
            "Plus tôt"

        En ->
            "Earlier"


later : Locale -> String
later locale =
    case locale of
        Fr ->
            "Plus tard"

        En ->
            "Later"


removeDrink : Locale -> String
removeDrink locale =
    case locale of
        Fr ->
            "Supprimer"

        En ->
            "Remove"


addDrink : Locale -> String
addDrink locale =
    case locale of
        Fr ->
            "Un verre\u{202F}?"

        En ->
            "A drink?"


yourApproximativeBloodAlcoholContentAt : Locale -> String
yourApproximativeBloodAlcoholContentAt locale =
    case locale of
        Fr ->
            "Ton taux d’alcool dans le sang est d’environ "

        En ->
            "Your blood alcohol content is around "


edit : Locale -> String
edit locale =
    case locale of
        Fr ->
            "Éditer"

        En ->
            "Edit"


ok : Locale -> String
ok _ =
    "OK"


youCanDrive : Locale -> String
youCanDrive locale =
    case locale of
        Fr ->
            "Tu peux conduire\u{202F}! (probablement)"

        En ->
            "You can drive! (probably)"


dontDrive : Locale -> String
dontDrive locale =
    case locale of
        Fr ->
            "Ne prends pas le volant."

        En ->
            "Don’t drive."


back : Locale -> String
back locale =
    case locale of
        Fr ->
            "Retour"

        En ->
            "Back"


delete : Locale -> String
delete locale =
    case locale of
        Fr ->
            "Supprimer"

        En ->
            "Delete"


save : Locale -> String
save locale =
    case locale of
        Fr ->
            "Enregistrer"

        En ->
            "Save"


at : Locale -> String
at locale =
    case locale of
        Fr ->
            "à"

        En ->
            "at"


abvUnit : Locale -> String
abvUnit locale =
    case locale of
        Fr ->
            "°"

        En ->
            "%\u{202F}ABV"


aStandardDrink : Locale -> String
aStandardDrink locale =
    case locale of
        Fr ->
            "Un verre standard"

        En ->
            "A standard drink"


howMuch : Locale -> String
howMuch locale =
    case locale of
        Fr ->
            "Combien\u{202F}?"

        En ->
            "How much?"


when : Locale -> String
when locale =
    case locale of
        Fr ->
            "Quand\u{202F}?"

        En ->
            "When?"


volume : Locale -> String
volume locale =
    case locale of
        Fr ->
            "Volume"

        En ->
            "Volume"


abv : Locale -> String
abv locale =
    case locale of
        Fr ->
            "Teneur en alcool"

        En ->
            "Alcohol by volume"
