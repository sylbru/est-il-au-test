module Icons exposing (..)

import Svg exposing (..)
import Svg.Attributes exposing (..)


ok : Svg msg
ok =
    svg
        [ width "1em"
        , height "1em"
        , viewBox "0 0 10 10"
        , strokeWidth "1"
        , stroke "currentColor"
        , fill "none"
        ]
        [ Svg.path [ d "M1,4 L4,7 L9,1" ] []
        ]


back : Svg msg
back =
    svg
        [ width "0.5em"
        , height "1em"
        , viewBox "0 0 5 10"
        , strokeWidth "1"
        , stroke "#fff"
        , fill "none"
        ]
        [ Svg.path [ d "M4.5,0 l-4,5 l4,5" ] []
        ]


delete : Svg msg
delete =
    svg
        [ width "1em"
        , height "1em"
        , viewBox "0 0 10 10"
        , strokeWidth "1"
        , stroke "#fff"
        , fill "none"
        ]
        [ Svg.path [ d "M1,1 l8,8 M1,9 l8,-8" ] []
        ]


close : Svg msg
close =
    delete


edit : Svg msg
edit =
    svg
        [ width "1em"
        , height "1em"
        , viewBox "0 0 10 10"
        ]
        [ Svg.rect [ x "3", y "1", width "6", height "8", rx "0.5", ry "0.5", strokeWidth "0.5", stroke "#fff", fill "none" ] []
        , Svg.path
            [ d "M1,9 l-0.5,-3 v-7 h1 v7 z"
            , transform "translate(8 0) rotate(-45 0 9)"
            , stroke "none"
            , fill "#fff"
            ]
            []
        ]


settings : Svg msg
settings =
    svg
        [ width "1em"
        , height "1em"
        , viewBox "0 0 10 10"
        , strokeWidth "1"
        , stroke "#fff"
        , fill "none"
        ]
        [ Svg.circle [ cx "5", cy "5", r "0.5", fill "currentColor" ] []
        , Svg.circle [ cx "5", cy "5", r "3" ] []
        , Svg.circle [ cx "5", cy "5", r "4", strokeDasharray "1" ] []
        ]


drink : Svg msg
drink =
    halfPint


halfPint : Svg msg
halfPint =
    svg
        [ width "2em"
        , height "2em"
        , viewBox "0 0 10 10"
        , strokeWidth "0.5"
        , stroke "#fff"
        , fill "none"
        ]
        [ Svg.path [ d "M3,1.5 l0.5,5 h2 l0.5,-5" ] []
        , Svg.path [ d "M3,2 l0.5,4.5 h2 l0.5,-4.5 z", fill beer, stroke "none" ] []
        , Svg.path [ d "M3,1.5 l0.3,0.5 h2.3 l0.3,-0.5", fill foam, stroke "none" ] []
        ]


pint : Svg msg
pint =
    svg
        [ width "2em"
        , height "2em"
        , viewBox "0 0 10 10"
        , strokeWidth "0.5"
        , stroke "#fff"
        , fill "none"
        ]
        [ Svg.path [ d "M2,0 l1,8 h4 l1,-8" ] []
        , Svg.path [ d "M2,1 l1,7 h4 l1,-7", fill beer, stroke "none" ] []
        , Svg.path [ d "M2,0 l0.2,1 h5.5 l0.2,-1", fill foam, stroke "none" ] []
        ]


beer : String
beer =
    "#fff5"


foam : String
foam =
    "#fffa"
