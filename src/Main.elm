port module Main exposing (init, main, timeToAttrValue, timeToString, update, view)

import Browser
import Browser.Dom exposing (Viewport)
import Density exposing (Density)
import Duration exposing (Duration)
import Element as El exposing (Attribute, Element)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as In exposing (OptionState)
import Element.Region exposing (description)
import Html exposing (Html)
import Html.Attributes as Attr
import Icons
import Json.Decode as Dec
import Json.Encode as Enc
import List.Extra exposing (maximumWith)
import Locale exposing (Locale(..))
import Mass exposing (Mass)
import Quantity exposing (Quantity, Rate)
import Svg exposing (Svg)
import Task
import Texts
import Time
import Volume exposing (Volume)


main : Program Flags Model Msg
main =
    Browser.element { init = init, update = update, view = view, subscriptions = subscriptions }


type alias BloodAlcoholContent =
    Rate Mass.Kilograms Volume.CubicMeters


type alias Model =
    { page : Page
    , drinks : List Drink
    , settings : { locale : Locale }
    , editDrinkVolumeInput : String
    , editDrinkAlcoholContentInput : String
    , now : Time.Posix
    , here : Time.Zone
    }


type alias Settings =
    { locale : Locale }


type alias StorableSettings =
    { locale : String }


type alias StorableDrinks =
    List StorableDrink


type alias StorableDrink =
    { id : DrinkId
    , when : Int
    , volume : Float
    , abv : Float
    }


type Msg
    = NewDrink Time.Posix
    | EditDrink Drink
    | EditDrinkTime Drink
    | EditDrinkAlcoholContent Drink
    | SaveChanges
    | CancelEditing
    | RemoveDrink DrinkId
    | SetEarlierDrinkTime Int
    | SetLaterDrinkTime Int
    | SetDrinkVolume String
    | SetDrinkAlcoholContent String
    | SetLocale Locale
    | OpenSettings
    | CloseSettings
    | GotTime Time.Posix
    | GotZone Time.Zone


type alias Drink =
    { id : DrinkId
    , when : Time.Posix
    , volume : Volume
    , abv : Quantity Float (Rate Volume.CubicMeters Volume.CubicMeters)
    }


type alias DrinkId =
    Int


type Page
    = Home
    | EditingDrink Drink
    | EditingDrinkTime Drink
    | EditingDrinkAlcoholContent Drink
    | SettingsPage


bloodAlcoholContentForDrink : Drink -> Quantity Float BloodAlcoholContent
bloodAlcoholContentForDrink drink =
    let
        alcoholVolume : Volume
        alcoholVolume =
            drink.volume
                |> Quantity.at drink.abv

        alcoholMass : Mass
        alcoholMass =
            alcoholVolume
                |> Quantity.at alcoholDensity

        standardDrinkUnit : Mass
        standardDrinkUnit =
            Mass.grams 10

        bloodAlcoholContentForStandardDrink : Quantity Float BloodAlcoholContent
        bloodAlcoholContentForStandardDrink =
            Quantity.rate
                (Mass.grams 0.25)
                (Volume.liters 1)
    in
    bloodAlcoholContentForStandardDrink
        |> Quantity.multiplyBy (Quantity.ratio alcoholMass standardDrinkUnit)


{-| Alcohol density (volumetric mass density).

Multiply a volume of alcohol by this constant
in order to get the mass of that volume of alcohol.

-}
alcoholDensity : Density
alcoholDensity =
    Quantity.rate (Mass.grams 8) (Volume.milliliters 10)


alcoholEliminationPerHour : Quantity Float (Rate BloodAlcoholContent Duration.Seconds)
alcoholEliminationPerHour =
    (Mass.grams 0.1 |> Quantity.per (Volume.liters 1))
        |> Quantity.per (Duration.hours 1)


oneHour : Int
oneHour =
    3600 * 1000


{-| The Algorithm.
-}
bloodAlcoholContent : { m | drinks : List Drink, now : Time.Posix } -> Quantity Float BloodAlcoholContent
bloodAlcoholContent { drinks, now } =
    let
        nowMillis =
            Time.posixToMillis now

        countDrink drink acc =
            let
                interval =
                    Duration.milliseconds (toFloat <| nowMillis - Time.posixToMillis drink.when)
            in
            acc
                |> Quantity.plus
                    (bloodAlcoholContentForDrink drink
                        |> Quantity.minus (alcoholEliminationPerHour |> Quantity.for interval)
                        |> Quantity.clamp Quantity.zero Quantity.infinity
                    )
    in
    List.foldl countDrink Quantity.zero drinks
        |> Quantity.clamp Quantity.zero Quantity.infinity


okToDrive : { m | drinks : List Drink, now : Time.Posix } -> Bool
okToDrive model =
    bloodAlcoholContent model |> Quantity.lessThan legalLimit


legalLimit : Quantity Float BloodAlcoholContent
legalLimit =
    Mass.grams 0.5
        |> Quantity.per (Volume.liters 1)


type alias Flags =
    Dec.Value


init : Flags -> ( Model, Cmd Msg )
init localStorageData =
    ( { page = Home
      , drinks =
            Dec.decodeValue (Dec.field "drinks" drinksDecoder) localStorageData
                |> Result.withDefault []
      , settings =
            Dec.decodeValue (Dec.field "settings" settingsDecoder) localStorageData
                |> Result.withDefault defaultSettings
      , editDrinkVolumeInput = ""
      , editDrinkAlcoholContentInput = ""
      , now = Time.millisToPosix 0
      , here = Time.utc
      }
    , Cmd.batch
        [ Task.perform GotZone Time.here
        , Task.perform GotTime Time.now
        ]
    )


discardOldDrinks : Time.Posix -> List Drink -> List Drink
discardOldDrinks now savedDrinks =
    let
        isOlderThan12Hours : Drink -> Bool
        isOlderThan12Hours drink =
            Time.posixToMillis drink.when >= Time.posixToMillis now - (12 * oneHour)
    in
    List.filter isOlderThan12Hours savedDrinks


settingsDecoder : Dec.Decoder Settings
settingsDecoder =
    Dec.field "locale" Locale.decoder
        |> Dec.map (\locale -> { locale = locale })


defaultSettings : Settings
defaultSettings =
    { locale = En }


drinksDecoder : Dec.Decoder (List Drink)
drinksDecoder =
    Dec.list drinkDecoder


drinkDecoder : Dec.Decoder Drink
drinkDecoder =
    Dec.map4
        (\id when volume abv ->
            { id = id, when = Time.millisToPosix when, volume = volume, abv = abv }
        )
        (Dec.field "id" Dec.int)
        (Dec.field "when" Dec.int)
        (Dec.field "volume" (Dec.map Volume.milliliters Dec.float))
        (Dec.field "abv" (Dec.map (\float -> Quantity.rate (Volume.milliliters float) (Volume.milliliters 100)) Dec.float))


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NewDrink when ->
            ( { model | page = EditingDrink (newDrink when model) }, Cmd.none )

        EditDrink drink ->
            ( { model | page = EditingDrink drink }, Cmd.none )

        EditDrinkTime drink ->
            ( { model | page = EditingDrinkTime drink }, Cmd.none )

        EditDrinkAlcoholContent drink ->
            ( { model
                | page = EditingDrinkAlcoholContent drink
                , editDrinkVolumeInput = inCentiliters drink.volume |> String.fromFloat
                , editDrinkAlcoholContentInput = drink.abv |> abvToString model.settings.locale
              }
            , Cmd.none
            )

        CancelEditing ->
            case model.page of
                EditingDrink _ ->
                    ( { model | page = Home }, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        SaveChanges ->
            case model.page of
                EditingDrink drink ->
                    let
                        newDrinks =
                            saveDrink drink model.drinks
                    in
                    ( { model | page = Home, drinks = newDrinks }, saveDrinks (drinksToStorable newDrinks) )

                _ ->
                    ( model, Cmd.none )

        RemoveDrink id ->
            let
                newDrinks =
                    List.filter (\d -> d.id /= id) model.drinks
            in
            ( { model | page = Home, drinks = newDrinks }, saveDrinks (drinksToStorable newDrinks) )

        SetEarlierDrinkTime deltaMillis ->
            case model.page of
                EditingDrinkTime drink ->
                    ( { model
                        | page = EditingDrinkTime (setEarlierDrinkTime deltaMillis model.now drink)
                      }
                    , Cmd.none
                    )

                _ ->
                    ( model, Cmd.none )

        SetLaterDrinkTime deltaMillis ->
            case model.page of
                EditingDrinkTime drink ->
                    ( { model
                        | page = EditingDrinkTime (setLaterDrinkTime deltaMillis model.now drink)
                      }
                    , Cmd.none
                    )

                _ ->
                    ( model, Cmd.none )

        SetDrinkVolume volumeString ->
            ( { model | editDrinkVolumeInput = volumeString }
                |> parseDrinkVolume
            , Cmd.none
            )

        SetDrinkAlcoholContent alcoholContentString ->
            ( { model | editDrinkAlcoholContentInput = alcoholContentString }
                |> parseDrinkAlcoholContent
            , Cmd.none
            )

        SetLocale locale ->
            let
                newSettings =
                    model.settings |> setLocale locale
            in
            ( { model | settings = newSettings }, saveSettings (settingsToStorable newSettings) )

        OpenSettings ->
            ( { model | page = SettingsPage }, Cmd.none )

        CloseSettings ->
            ( { model | page = Home }, Cmd.none )

        GotTime posixTime ->
            ( { model
                | now = posixTime
                , drinks =
                    if model.now == Time.millisToPosix 0 then
                        -- First time we got the current time,
                        -- we want to discard saved drinks if they are older than 12h
                        model.drinks |> discardOldDrinks posixTime

                    else
                        model.drinks
              }
            , Cmd.none
            )

        GotZone timeZone ->
            ( { model | here = timeZone }, Cmd.none )


settingsToStorable : Settings -> StorableSettings
settingsToStorable settings =
    { locale = Locale.toString settings.locale }


drinksToStorable : List Drink -> StorableDrinks
drinksToStorable drinks =
    List.map
        (\drink ->
            { id = drink.id
            , when = Time.posixToMillis drink.when
            , volume = Volume.inMilliliters drink.volume
            , abv = drink.abv |> Quantity.for (Volume.milliliters 100) |> Volume.inMilliliters
            }
        )
        drinks


setLocale : Locale -> { locale : Locale } -> { locale : Locale }
setLocale locale settings =
    { settings | locale = locale }


saveDrink : Drink -> List Drink -> List Drink
saveDrink drink drinks =
    if List.any (\d -> d.id == drink.id) drinks then
        List.map
            (\d ->
                if d.id == drink.id then
                    drink

                else
                    d
            )
            drinks

    else
        drink :: drinks


changeDrinkTime : (Int -> Int -> Int) -> Int -> Time.Posix -> Drink -> Drink
changeDrinkTime operation step now drink =
    let
        newTime =
            drink.when
                |> Time.posixToMillis
                |> (\millis -> operation millis step)
                |> min (roundTime step now |> Time.posixToMillis)
                |> Time.millisToPosix
    in
    { drink | when = newTime }


minutes : Int -> Int
minutes m =
    m * 60 * 1000


fiveMinutes : Int
fiveMinutes =
    minutes 5


setEarlierDrinkTime : Int -> Time.Posix -> Drink -> Drink
setEarlierDrinkTime stepMillis now =
    changeDrinkTime (-) stepMillis now


setLaterDrinkTime : Int -> Time.Posix -> Drink -> Drink
setLaterDrinkTime stepMillis now =
    changeDrinkTime (+) stepMillis now


parseDrinkVolume : Model -> Model
parseDrinkVolume model =
    case model.page of
        EditingDrinkAlcoholContent drink ->
            { model
                | page =
                    EditingDrinkAlcoholContent (setDrinkVolume model.editDrinkVolumeInput drink)
            }

        _ ->
            model


parseDrinkAlcoholContent : Model -> Model
parseDrinkAlcoholContent model =
    case model.page of
        EditingDrinkAlcoholContent drink ->
            { model
                | page =
                    EditingDrinkAlcoholContent (setDrinkAlcoholContent model.editDrinkAlcoholContentInput drink)
            }

        _ ->
            model


setDrinkVolume : String -> Drink -> Drink
setDrinkVolume volumeString drink =
    let
        maybeVolume =
            String.toInt volumeString
    in
    case maybeVolume of
        Just volume ->
            { drink | volume = Volume.milliliters (toFloat volume * 10) }

        Nothing ->
            drink


setDrinkAlcoholContent : String -> Drink -> Drink
setDrinkAlcoholContent alcoholContentString drink =
    let
        maybeAlcoholContent =
            String.toFloat alcoholContentString
                |> Maybe.map (\f -> f / 100)
    in
    case maybeAlcoholContent of
        Just alcoholContent ->
            { drink | abv = Quantity.rate (Volume.liters alcoholContent) (Volume.liters 1) }

        Nothing ->
            drink


newDrink : Time.Posix -> Model -> Drink
newDrink when model =
    let
        maybeMaxId =
            List.map .id model.drinks
                |> List.maximum

        newId =
            case maybeMaxId of
                Just maxId ->
                    maxId + 1

                Nothing ->
                    1
    in
    { id = newId, when = when, volume = defaultVolume, abv = defaultAbv }


abvToString : Locale -> Quantity Float (Rate Volume.CubicMeters Volume.CubicMeters) -> String
abvToString locale abv =
    abv
        |> Quantity.in_ (Volume.cubicMeters >> Quantity.per Volume.cubicMeter)
        |> (*) 100
        |> toFixed 1 locale


{-| Default quantity/volume
-}
defaultVolume : Volume
defaultVolume =
    Volume.milliliters 250


{-| Default ABV (alcohol by volume)

Defined as the ratio of alcohol volume per total volume.
Given a value in ° or %vol, be careful to divide that value by 100.

-}
defaultAbv : Quantity Float (Rate Volume.CubicMeters Volume.CubicMeters)
defaultAbv =
    Quantity.rate
        (Volume.milliliters 5)
        (Volume.milliliters 100)


defaultAlcoholContent : Float
defaultAlcoholContent =
    1


view : Model -> Html Msg
view model =
    El.layout
        [ El.height El.fill
        , Background.color
            (if okToDrive model then
                darker green

             else
                darker red
            )
        , Font.color (El.rgb 1 1 1)
        , Font.size defaultFontSize
        , Font.family [ Font.sansSerif ]
        , Font.center
        , transitionBackgroundColor
        , transitionBackgroundImage
        ]
        (El.column
            (appStyle model)
            [ body model
            , footer model
            ]
        )


appStyle : Model -> List (Attribute Msg)
appStyle model =
    [ El.height El.fill
    , El.centerX
    , El.width (El.fill |> El.maximum 800)
    , El.padding viewportSpacing
    , Background.color
        (if okToDrive model then
            green

         else
            red
        )
    , if okToDrive model then
        Background.gradient { angle = degrees 160, steps = [ green, green, darker green ] }

      else
        Background.gradient { angle = degrees 160, steps = [ red, red, darker red ] }
    , Border.glow (El.rgb 0.1 0.1 0.1) 5
    , transitionBackgroundColor
    , transitionBackgroundImage
    ]


transitionColor : Attribute msg
transitionColor =
    El.htmlAttribute <| Attr.style "transition" "color 0.5s"


transitionBackgroundColor : Attribute msg
transitionBackgroundColor =
    El.htmlAttribute <| Attr.style "transition" "background-color 2s"


transitionBackgroundImage : Attribute msg
transitionBackgroundImage =
    El.htmlAttribute <| Attr.style "transition" "background-image 2s"


body : Model -> Element Msg
body model =
    case model.page of
        Home ->
            home model

        EditingDrink drink ->
            editScreen model drink

        EditingDrinkTime drink ->
            editTimeScreen model drink

        EditingDrinkAlcoholContent drink ->
            editAlcoholContentScreen model drink

        SettingsPage ->
            settingsScreen model


home : Model -> Element Msg
home model =
    El.column
        [ El.height El.fill, El.width El.fill, El.spacing 20 ]
        [ El.row
            [ El.paddingEach { top = viewportSpacing, left = 0, right = 0, bottom = 0 }
            , El.height <| El.fillPortion 1
            , El.width El.fill
            ]
            [ In.button
                (El.centerX
                    :: heavyButtonStyle
                        (if okToDrive model then
                            green

                         else
                            red
                        )
                )
                { onPress = Just <| NewDrink (roundTime fiveMinutes model.now)
                , label = adjustTextY <| Texts.addDrink model.settings.locale
                }
            ]
        , if List.isEmpty model.drinks then
            El.paragraph
                [ El.height <| El.fillPortion 1, Font.center ]
                [ El.text <| Texts.iHaveHadNoDrinks model.settings.locale ]

          else
            El.none
        , viewDrinks model
        , El.el [ El.height <| El.fillPortion 1, El.padding 5, El.width El.fill, Font.size (defaultFontSize + 2) ]
            (El.paragraph [ El.centerY, El.centerX ] <|
                [ El.text (Texts.yourApproximativeBloodAlcoholContentAt model.settings.locale) ]
            )
        , El.el [ El.centerX, Font.size (defaultFontSize + 6), Font.bold ]
            (El.text <|
                (bloodAlcoholContent model
                    |> Quantity.in_ (Mass.grams >> Quantity.per Volume.liter)
                    |> toFixed 2 model.settings.locale
                )
                    ++ " g/l"
            )
        , El.el [ El.height <| El.fillPortion 2, El.width El.fill ]
            (El.paragraph [ El.centerX, El.centerY, Font.size (defaultFontSize + 3), Font.bold ]
                [ El.text <|
                    if okToDrive model then
                        Texts.youCanDrive model.settings.locale

                    else
                        Texts.dontDrive model.settings.locale
                ]
            )
        ]


editScreen : Model -> Drink -> Element Msg
editScreen model drink =
    let
        color =
            if okToDrive model then
                green

            else
                red

        isNew =
            model.drinks
                |> List.map .id
                |> (not << List.member drink.id)

        subScreenButtonStyle =
            [ El.centerX
            , El.padding 30
            , Border.color (El.rgba 1 1 1 0.75)
            , Border.width 2
            , Border.rounded 10
            , El.mouseOver [ Background.color <| El.rgba 1 1 1 0.1 ]
            ]
    in
    El.column
        [ El.height El.fill
        , El.width El.fill
        ]
        [ editScreenNav drink isNew model.settings.locale color
        , El.column
            [ realSpaceEvenly
            , El.height El.fill
            , El.width El.fill
            ]
            [ In.button subScreenButtonStyle
                { onPress = Just <| EditDrinkAlcoholContent drink
                , label =
                    El.column
                        [ El.spacing 20 ]
                        [ El.el [ El.centerX, Font.bold ] (El.text <| Texts.howMuch model.settings.locale)
                        , El.el [ El.centerX ] (El.text <| alcoholContentToString { volume = drink.volume, abv = drink.abv } model.settings.locale)
                        ]
                }
            , In.button subScreenButtonStyle
                { onPress = Just <| EditDrinkTime drink
                , label =
                    El.column
                        [ El.spacing 20, El.width El.fill ]
                        [ El.el [ El.centerX, Font.bold ] (El.text <| Texts.when model.settings.locale)
                        , El.el [ El.centerX ] (El.text <| timeToString model.here drink.when model.settings.locale)
                        ]
                }
            ]
        ]


alcoholContentToString : { volume : Volume, abv : Quantity Float (Rate Volume.CubicMeters Volume.CubicMeters) } -> Locale -> String
alcoholContentToString ({ volume, abv } as alcoholContent) locale =
    let
        standardDrink : { volume : Volume, abv : Quantity Float (Rate Volume.CubicMeters Volume.CubicMeters) }
        standardDrink =
            { volume = defaultVolume, abv = defaultAbv }
    in
    if alcoholContent == standardDrink then
        Texts.aStandardDrink locale

    else
        let
            volumeString =
                inCentiliters volume
                    |> String.fromFloat
                    |> append (" " ++ volumeUnit)

            abvString =
                abv
                    |> abvToString locale
                    |> append (Texts.abvUnit locale)
        in
        String.join " " [ volumeString, Texts.at locale, abvString ]


append : String -> String -> String
append suffix input =
    input ++ suffix


volumeUnit : String
volumeUnit =
    "cl"


editTimeScreen : Model -> Drink -> Element Msg
editTimeScreen model drink =
    El.column
        [ El.height El.fill
        , El.width El.fill
        ]
        [ editSubScreenNav (EditDrink drink)
            (if okToDrive model then
                green

             else
                red
            )
        , editTimeActions model.here model.settings.locale drink
        ]


editAlcoholContentScreen : Model -> Drink -> Element Msg
editAlcoholContentScreen model drink =
    El.column
        [ El.height El.fill
        , El.width El.fill
        ]
        [ editSubScreenNav (EditDrink drink)
            (if okToDrive model then
                green

             else
                red
            )
        , editAlcoholContentActions model.settings.locale drink model.editDrinkVolumeInput model.editDrinkAlcoholContentInput
        ]


editScreenNav : Drink -> Bool -> Locale -> El.Color -> Element Msg
editScreenNav drink isNew locale color =
    El.wrappedRow
        [ El.width El.fill, El.spaceEvenly ]
        [ In.button lightButtonStyle
            { onPress = Just CancelEditing
            , label = El.row [ El.spacing 10 ] [ El.el [] (El.html <| Icons.back), adjustTextY <| Texts.back locale ]
            }
        , if not isNew then
            In.button lightButtonStyle
                { onPress = Just <| RemoveDrink drink.id
                , label = El.row [ El.spacing 10 ] [ El.el [] (El.html <| Icons.delete), adjustTextY <| Texts.delete locale ]
                }

          else
            El.none
        , In.button (heavyButtonStyle color) { onPress = Just SaveChanges, label = adjustTextY <| Texts.save locale }
        ]


editSubScreenNav : Msg -> El.Color -> Element Msg
editSubScreenNav backMsg color =
    El.wrappedRow
        [ El.width El.fill ]
        [ In.button (El.centerX :: heavyButtonStyle color)
            { onPress = Just backMsg
            , label =
                El.row [ El.spacing 10 ] [ El.el [] (El.html <| Icons.ok), adjustTextY "OK" ]
            }
        ]


adjustTextY : String -> Element msg
adjustTextY text =
    El.el [ El.htmlAttribute <| Attr.style "transform" "translateY(.05em)" ] (El.text <| text)


editTimeActions : Time.Zone -> Locale -> Drink -> Element Msg
editTimeActions zone locale drink =
    let
        timeInput =
            Html.input
                [ Attr.type_ "text" -- time input seems basically unstylable
                , Attr.style "background" "inherit"
                , Attr.style "font-size" "inherit"
                , Attr.style "color" "#fff"
                , Attr.style "border" "none"
                , Attr.style "text-align" "center"
                ]
                []
    in
    El.column
        [ El.width El.fill, El.height El.fill, realSpaceEvenly ]
        [ El.el [ El.centerX ] <| El.text (Texts.aDrinkAtColon locale)
        , El.wrappedRow
            [ El.centerX, realSpaceEvenly, El.width El.fill ]
            [ In.button lightButtonStyle { onPress = Just <| SetEarlierDrinkTime (minutes 30), label = El.text <| "−30" }
            , In.button lightButtonStyle { onPress = Just <| SetEarlierDrinkTime (minutes 5), label = El.text <| "−5" }
            , El.el
                [ El.width <| El.px 120
                , Border.width 1
                , Border.rounded 10
                , El.padding 15
                ]
                (El.text (timeToString zone drink.when locale))
            , In.button lightButtonStyle { onPress = Just <| SetLaterDrinkTime (minutes 5), label = El.text <| "+5" }
            , In.button lightButtonStyle { onPress = Just <| SetLaterDrinkTime (minutes 30), label = El.text <| "+30" }
            ]
        ]


inCentiliters : Volume -> Float
inCentiliters volume =
    volume
        |> Volume.inMilliliters
        |> (\ml -> ml / 10)
        |> round
        |> toFloat


editAlcoholContentActions : Locale -> Drink -> String -> String -> Element Msg
editAlcoholContentActions locale drink volumeInputText alcoholContentInputText =
    let
        inputStyle : List (El.Attribute Msg)
        inputStyle =
            [ El.padding 7
            , Border.width 2
            , Border.rounded 10
            , Border.color (El.rgba 1 1 1 0.9)
            , El.width (El.px 100)
            , Background.color (El.rgba 0 0 0 0)
            ]

        unitWidth : El.Length
        unitWidth =
            El.px 100
    in
    El.column
        [ El.width El.fill, El.height El.fill, realSpaceEvenly, El.padding 50 ]
        [ El.column [ El.width El.fill ]
            [ El.row [ El.width El.fill, El.spaceEvenly ]
                [ El.el [ Font.bold ] (El.text <| Texts.volume locale)
                , El.row [ El.spacing 20 ]
                    [ In.text inputStyle
                        { onChange = SetDrinkVolume
                        , text = volumeInputText
                        , label = In.labelHidden <| Texts.volume locale ++ " (" ++ volumeUnit ++ ")"
                        , placeholder = Nothing
                        }
                    , El.el [ El.width unitWidth, Font.alignLeft ] (El.text volumeUnit)
                    ]
                ]
            ]
        , El.column [ El.width El.fill ]
            [ El.row [ El.width El.fill, El.spaceEvenly ]
                [ El.el [ Font.bold ] (El.text <| Texts.abv locale)
                , El.row [ El.spacing 20 ]
                    [ In.text inputStyle
                        { onChange = SetDrinkAlcoholContent
                        , text = alcoholContentInputText
                        , label = In.labelHidden <| Texts.abv locale ++ " (" ++ Texts.abvUnit locale ++ ")"
                        , placeholder = Nothing
                        }
                    , El.el [ El.width unitWidth, Font.alignLeft ] (El.text (Texts.abvUnit locale))
                    ]
                ]
            ]
        ]


settingsScreen : Model -> Element Msg
settingsScreen model =
    El.column
        [ El.height El.fill, El.padding viewportSpacing, El.width El.fill ]
        [ El.row [ El.centerY, El.centerX, El.width El.fill ]
            [ In.radioRow [ realSpaceEvenly, El.width El.fill ]
                { onChange = SetLocale
                , selected = Just model.settings.locale
                , label = In.labelHidden "Language"
                , options =
                    [ In.optionWith Fr (customOptionFor "Français")
                    , In.optionWith En (customOptionFor "English")
                    ]
                }
            ]
        ]


customOptionFor : String -> OptionState -> Element msg
customOptionFor label optionState =
    El.text <|
        (if optionState == In.Selected then
            "☒ "

         else
            "☐ "
        )
            ++ label


footer : Model -> Element Msg
footer model =
    let
        ( msg, svgIcon ) =
            case model.page of
                SettingsPage ->
                    ( CloseSettings, El.html <| Icons.close )

                _ ->
                    ( OpenSettings, El.html <| Icons.settings )
    in
    El.row
        [ El.padding viewportSpacing, El.centerX, El.height El.shrink ]
        [ In.button lightButtonStyle
            { onPress = Just msg
            , label = svgIcon
            }
        ]


viewDrinks : Model -> Element Msg
viewDrinks model =
    El.column
        [ El.height <| El.fillPortion 4
        , El.scrollbarY
        , El.width El.fill
        , El.padding 3
        , Border.widthEach { top = 0, left = 0, right = 0, bottom = 1 }
        ]
        (model.drinks
            |> List.sortBy (.when >> Time.posixToMillis)
            |> List.map
                (viewDrink model.here model.settings.locale)
        )


viewDrink : Time.Zone -> Locale -> Drink -> Element Msg
viewDrink timezone locale drink =
    let
        drinkText : String
        drinkText =
            Texts.aDrinkAt locale ++ timeToString timezone drink.when locale
    in
    El.wrappedRow
        [ El.width El.fill
        ]
        [ In.button
            [ El.centerX, El.mouseOver [ Background.color (El.rgba 1 1 1 0.1) ], Border.rounded 10, El.height (El.px 50) ]
            { onPress = Just <| EditDrink drink
            , label =
                El.row [ El.paddingEach { right = 10, top = 0, left = 0, bottom = 0 } ]
                    [ icon Icons.drink
                    , El.el [ El.width <| El.fillPortion 3, El.centerX ] (El.text drinkText)
                    ]
            }
        ]


timeToAttrValue : Time.Zone -> Time.Posix -> String
timeToAttrValue timezone time =
    (String.fromInt (Time.toHour timezone time) |> String.padLeft 2 '0')
        ++ ":"
        ++ (String.fromInt (Time.toMinute timezone time) |> String.padLeft 2 '0')


roundTime : Int -> Time.Posix -> Time.Posix
roundTime step time =
    let
        timeMillis =
            Time.posixToMillis time
    in
    (toFloat timeMillis / toFloat step)
        |> round
        |> (*) step
        |> Time.millisToPosix


timeToString : Time.Zone -> Time.Posix -> Locale -> String
timeToString zone time locale =
    let
        inMinutes =
            Time.toMinute zone time

        minutesString =
            if inMinutes > 0 then
                inMinutes |> String.fromInt |> String.padLeft 2 '0'

            else
                ""
    in
    case locale of
        Fr ->
            (Time.toHour zone time |> String.fromInt)
                ++ "h"
                ++ minutesString

        En ->
            let
                hours =
                    Time.toHour zone time

                pm =
                    if hours >= 12 then
                        "pm"

                    else
                        "am"

                hoursString =
                    remainderBy 12 hours
                        -- midnight is 12am instead of 0am
                        |> (\h ->
                                if h == 0 then
                                    12

                                else
                                    h
                           )
                        |> String.fromInt
            in
            hoursString
                ++ (if String.isEmpty minutesString then
                        ""

                    else
                        ":" ++ minutesString
                   )
                ++ pm


subscriptions : Model -> Sub Msg
subscriptions model =
    Time.every (30 * 1000) GotTime


mapIf : (a -> Bool) -> (a -> a) -> List a -> List a
mapIf conditionFunction mapFunction list =
    List.map
        (\a ->
            if conditionFunction a then
                mapFunction a

            else
                a
        )
        list


toFixed : Int -> Locale -> Float -> String
toFixed precision locale x =
    let
        toLocale =
            case locale of
                Fr ->
                    String.replace "." ","

                En ->
                    identity
    in
    x
        * toFloat (10 ^ precision)
        |> round
        |> toFloat
        |> (\n -> n / toFloat (10 ^ precision))
        |> String.fromFloat
        |> toLocale



-- STYLE --


defaultFontSize : Int
defaultFontSize =
    20


viewportSpacing : Int
viewportSpacing =
    10


generalSpacing : Int
generalSpacing =
    30


lightButtonStyle : List (Attribute msg)
lightButtonStyle =
    [ El.padding 10 ]


heavyButtonStyle : El.Color -> List (Attribute msg)
heavyButtonStyle textColor =
    [ Font.bold
    , El.padding 15
    , Font.color textColor
    , Background.color white
    , Border.rounded (round <| toFloat defaultFontSize * 1.5)
    , transitionColor
    ]


green : El.Color
green =
    El.rgb255 0x38 0x81 0x5C


red : El.Color
red =
    El.rgb255 0x68 0x19 0x16


white : El.Color
white =
    El.rgb 1 1 1


black : El.Color
black =
    El.rgb 0.1 0.1 0.1


darker : El.Color -> El.Color
darker =
    darkerBy 0.7


darkerBy : Float -> El.Color -> El.Color
darkerBy rate color =
    let
        darkenChannel : Float -> Float
        darkenChannel channel =
            channel * rate
    in
    El.toRgb color
        |> (\rgb ->
                { red = darkenChannel rgb.red
                , green = darkenChannel rgb.green
                , blue = darkenChannel rgb.blue
                , alpha = rgb.alpha
                }
           )
        |> El.fromRgb


realSpaceEvenly : Attribute msg
realSpaceEvenly =
    El.htmlAttribute <| Attr.style "justify-content" "space-evenly"


icon : Svg msg -> El.Element msg
icon svgIcon =
    El.html svgIcon



-- PORTS --


port saveSettings : StorableSettings -> Cmd msg


port saveDrinks : StorableDrinks -> Cmd msg
