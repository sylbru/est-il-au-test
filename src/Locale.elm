module Locale exposing (..)

import Json.Decode as Dec exposing (Decoder)


type Locale
    = Fr
    | En


toString : Locale -> String
toString locale =
    case locale of
        Fr ->
            "fr"

        En ->
            "en"


decoder : Decoder Locale
decoder =
    Dec.map
        (\localeString ->
            if String.toLower localeString == "fr" then
                Fr

            else
                En
        )
        Dec.string
