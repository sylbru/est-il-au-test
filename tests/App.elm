module App exposing (..)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, list, string)
import Json.Encode
import Main
import ProgramTest
import Test exposing (..)
import Test.Html.Selector exposing (text)


suite : Test
suite =
    describe "Integration tests"
        [ initialLoad
        , oneStandardDrink

        --, manyOldDrinksPlusOneRecentDrink
        ]


initialLoad : Test
initialLoad =
    test "No alcohol on initial load" <|
        \_ ->
            ProgramTest.createElement { init = Main.init, update = Main.update, view = Main.view }
                |> ProgramTest.start Json.Encode.null
                |> ProgramTest.expectViewHas [ text "0 g/l" ]


oneStandardDrink : Test
oneStandardDrink =
    test "One standard drink, 0.25 g/l" <|
        \_ ->
            ProgramTest.createElement { init = Main.init, update = Main.update, view = Main.view }
                |> ProgramTest.start Json.Encode.null
                |> ProgramTest.clickButton "A drink?"
                |> ProgramTest.clickButton "Enregistrer"
                |> ProgramTest.expectViewHas [ text "0.25 g/l" ]



--manyOldDrinksPlusOneRecentDrink : Test
--manyOldDrinksPlusOneRecentDrink =
--    test "Many old drinks, plus one recent drink, 0.25 g/l" <|
--        \_ ->
--            ProgramTest.createElement { init = Main.init, update = Main.update, view = Main.view }
--                |> ProgramTest.start Json.Encode.null
--                |> ProgramTest.clickButton "A drink?"
--clickButton : String -> ProgramTest model msg effect -> ProgramTest model msg effect
--clickButton label =
--    ProgramTest.simulateDomEvent
--        (Test.Html.Query.findAll [ checkbox label ] >> Test.Html.Query.first)
--        Test.Html.Event.click
