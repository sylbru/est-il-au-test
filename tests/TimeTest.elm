module TimeTest exposing (..)

import Expect
import Locale
import Main exposing (timeToAttrValue, timeToString)
import Test exposing (Test, describe, test)
import Time


suite : Test
suite =
    describe "Unit tests on time functions"
        [ timeToStringTests
        , timeToAttrValueTests
        ]


timeToStringTests : Test
timeToStringTests =
    let
        time2147 =
            Time.millisToPosix 1659131266000
    in
    describe "timeToString"
        [ test "fr" <|
            \_ -> Expect.equal (timeToString Time.utc time2147 Locale.Fr) "21h47"
        , test "en" <|
            \_ -> Expect.equal (timeToString Time.utc time2147 Locale.En) "9:47pm"
        ]


timeToAttrValueTests : Test
timeToAttrValueTests =
    let
        time2147 =
            Time.millisToPosix 1659131266000

        time0108 =
            Time.millisToPosix 1659056926000

        time0026 =
            Time.millisToPosix 1659054406000
    in
    describe "timeToAttrValue"
        [ test "four digits" <|
            \_ -> Expect.equal (timeToAttrValue Time.utc time2147) "21:47"
        , test "three digits" <|
            \_ -> Expect.equal (timeToAttrValue Time.utc time0108) "01:08"
        , test "two digits" <|
            \_ -> Expect.equal (timeToAttrValue Time.utc time0026) "00:26"
        ]
