# Est-il au test ?

Éthylotest approximatif sur la base d’une déclaration sur l’honneur du nombre
et du type de verres d’alcool consommés, avec les heures de consommation,
permettant d’estimer le niveau d’alcoolémie à une certaine heure.

* Développement : `npm run dev` ou `vite`
* Build : `npm run build` ou `vite build`
