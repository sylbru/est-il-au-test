import { Elm } from "./src/Main.elm" 
 
let settings;
try {
    settings = JSON.parse(window.localStorage.getItem("settings"))
} catch (e) {
    console.log("Corrupted settings: " + window.localStorage.getItem("settings"))
    settings = null
}

// Check preferred default language if no settings were found
if (settings === null) {
    settings = {
        locale: navigator.language.match(/^fr([\-_][A-Z]+)?/)
            ? "fr"
            : "en"
        }
}    

let drinks;
try {
    drinks = JSON.parse(window.localStorage.getItem("drinks"))
} catch (e) {
    console.log("Corrupted drinks data: " + window.localStorage.getItem("drinks"))
    drinks = null
}

const app = Elm.Main.init({
    node: document.getElementById("app"),
    flags: { settings, drinks },
})

app.ports.saveSettings.subscribe(function (data) {
    window.localStorage.setItem("settings", JSON.stringify(data))
})

app.ports.saveDrinks.subscribe(function (data) {
    window.localStorage.setItem("drinks", JSON.stringify(data))
})